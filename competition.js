/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module('myApp', []);
        
// Controller les compétitions
app.controller('competitionsCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) { 
    $http.get('http://localhost:8084/FBLBack/rest/competitions').then(function (response) {$scope.competitions = response.data;});
}]);

// Controller une compétition
app.controller('competitionCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) { 
    $http.get('http://localhost:8084/FBLBack/rest/competitions?id='+$scope.id_competition).then(function (response) {$scope.competition = response.data;});
}]);