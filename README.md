# Projet F-BL


## Présentation du projet
Le projet FBL est un projet de cours pour dans le cadre de nos études en Master 2 MIAGE par appprentissage.
Le but de ce projet est de montrer les capacités d'une API REST en fournissant un client web (Angularjs) et un serveur (Java Spring) communiquant et échangeant des données isusues d'une base de données PostgreSQL.

Nous avons décidé de réaliser un site internet de paris entre amis à la manière de championnats avec un certains nombre de paris entre les joueurs réunis dans une même ligue.
On peut ainsi voir les développements suivants :
- Création d'une base de données avec la gestion :
    - Des utilisateurs
    - Des Ligues pour les paris des utilisateurs
    - Des matchs
    - Des compétitions réelles dans lesquelles les matchs sont joués
    - Des équipes participants aux matchs et donc à ces compétitions
    - Des catégories de compétitions (Coupe, Championnat, etc.)
    - Des types et sous_types de matchs associés aux compétitions (Sport, E-Sport, Hockey, Football, etc.)
- Création de toute la partie serveur pour la gestion et l'exploitation des données issues de la base
- D'une interface utilisateur pour le rendu des différentes informations utiles (les matchs disponibles, les équipes disponibles, les paris effectués, les ligues disponibles, les résultats des matchs, etc.)

## Description des API

### Pour les users

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getUsers () | GET | http://localhost:8084/FBLBack/rest/users | Récupération des tous les utilisateurs de la base | - |
| getUsers (id) | GET | http://localhost:8084/FBLBack/rest/users?id=x | Récupération de l'utilisateur aillant pour id celui choisi dans le lien | id : integer |
| getUsers (login, mdp) | GET | http://localhost:8084/FBLBack/rest/users?login=xx&mdp=xx | Récupération de l'utilisateur en fonction de son login et son mot de passe | login : String <br> mdp : String |
| getUsers (mail, mdp) | GET | http://localhost:8084/FBLBack/rest/users?mail=xx&mdp=xx | Récupération de l'utilisateur en fonction de son mail et son mot de passe | mail : String <br> mdp : String |

### Pour les catégories des compétitions 

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getCategorie_Competition () | GET | http://localhost:8084/FBLBack/rest/categories | Récupération de la liste des compétitions enregistrées | - |
| getCategorie_Competition (id) | GET | http://localhost:8084/FBLBack/rest/categories?id=x | Catégorie de compétition avec l'id choisi | id : integer |
| getCategorie_Competition (nom) | GET | http://localhost:8084/FBLBack/rest/categories?nom=xx | Catégorie de compétition avec le nom saisi | nom : String |

### Pour les types de matchs

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getType_match () | GET | http://localhost:8084/FBLBack/rest/types | Récupération de la liste des types de matchs enregistrées | - |
| getType_match (id) | GET | http://localhost:8084/FBLBack/rest/types?id=x | Type général de match avec l'id choisi | id : integer |
| getType_match (nom) | GET | http://localhost:8084/FBLBack/rest/types?nom=xx | Type général de match avec le nom saisi | nom : String |

### Pour les sous-types de match

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getSous_Type_match () | GET | http://localhost:8084/FBLBack/rest/sous_type | Récupération de la liste des types de matchs précis (Handball, etc.) enregistrées | - |
| getSous_Type_matchId (id) | GET | http://localhost:8084/FBLBack/rest/sous_type?id=x | Sous type de match avec l'id choisi | id : integer |
| getSous_Type_match (nom) | GET | http://localhost:8084/FBLBack/rest/sous_type?nom=xx | Sous type de match avec le nom choisi | nom : String |
| getSous_Type_match (type) | GET | http://localhost:8084/FBLBack/rest/sous_type?type=xx | Liste des sous types de match aillant pour type le type choisi | type : int |

### Pour les compétitions

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getCompetition () | GET | http://localhost:8084/FBLBack/rest/competitions | Récupération de la liste des compétitions sans paramètre | - |
| getCompetition (id) | GET | http://localhost:8084/FBLBack/rest/competitions?id=x | Récupération des informations pour la compétition avec l'ID saisi | id : integer |
| getCompetition (nom) | GET | http://localhost:8084/FBLBack/rest/competitions?nom=xx | Récupération des informations pour la compétition avec le nom saisi | nom : String |
| getCompetitionCat (cat) | GET | http://localhost:8084/FBLBack/rest/competitions?cat=xx | Liste de toutes les compétitions d'une catégorie | cat : int |
| getCompetitionSType (soustype) | GET | http://localhost:8084/FBLBack/rest/competitions?soustype=xx | Liste de toutes les compétitions d'un sous-type | soustype : int |
| getCompetitionType (type) | GET | http://localhost:8084/FBLBack/rest/competitions?type=xx | Liste de toutes les compétitions d'un type de match | type : int |

### Pour les équipes

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getEquipes () | GET | http://localhost:8084/FBLBack/rest/equipes | Liste de toutes les équipes de la base | - |
| getEquipes (id) | GET | http://localhost:8084/FBLBack/rest/equipes?id=x | Equipe avec l'ID saisi | id : integer |
| getEquipes (nom) | GET | http://localhost:8084/FBLBack/rest/equipes?nom=xx | Equipe avec le nom saisi | nom : String |

### Pour les matchs

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getMatchs () | GET | http://localhost:8084/FBLBack/rest/matchs | Récupération de la liste des matchs | - |
| getMatchs (id) | GET | http://localhost:8084/FBLBack/rest/matchs?id=x | Récupération du match avec l'id saisi | id : integer |
| getMatchs (date) | GET | http://localhost:8084/FBLBack/rest/matchs?date=xx | Récupération des matchs entrés à la date choisie | date : Date |
| getMatchsCompetition (competition) | GET | http://localhost:8084/FBLBack/rest/matchs?competition=xx | Liste des matchs de la compétition choisie | competition : int |
| getMatchsCategorie (categorie) | GET | http://localhost:8084/FBLBack/rest/matchs?categorie=xx | Liste des matchs de la catégorie choisie | categorie : int |
| getMatchsSousType (soustype) | GET | http://localhost:8084/FBLBack/rest/matchs?soustype=xx | Liste des matchs du sous type choisi | soustype : int |
| getMatchsType (type) | GET | http://localhost:8084/FBLBack/rest/matchs?type=xx | Liste des matchs du type choisi | type : int |

### Pour les ligues

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getLigues () | GET | http://localhost:8084/FBLBack/rest/ligues | Liste des ligues | - |
| getLigues (id) | GET | http://localhost:8084/FBLBack/rest/ligues?id=x | Informations sur la ligue avec l'id choisi| id : integer |
| getLigues (nom) | GET | http://localhost:8084/FBLBack/rest/ligues?nom=xx | Informations sur la ligue avec le nom choisi | nom : String |

### Pour la gestion des points 

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getPoints () | GET | http://localhost:8084/FBLBack/rest/points | Récupération de la liste de tous les paris effectués | - |
| getPointsUser (id) | GET | http://localhost:8084/FBLBack/rest/points?user=x | Récupération des paris effectués par l'utilisateur saisi  | user : int |
| getPointsLigue (ligue) | GET | http://localhost:8084/FBLBack/rest/points?ligue=xx | Récupération des paris effectués dans la ligue saisie | ligue : int |
| getPointsMatch (match) | GET | http://localhost:8084/FBLBack/rest/points?match=xx | Récupération des paris effectués sur le match saisi | match : int |
| getPointsMatch (user,ligue) | GET | http://localhost:8084/FBLBack/rest/points?user=xx&ligue=xx | Récupération des paris effectués par un utilisateur dans une ligue | user : int <br>ligue : int|
| setPoints (user, ligue, match, score1, score2) | POST | http://localhost:8084/FBLBack/rest/points?user=x&ligue=x&match=x&score1=x&score2=x | Création d'un nouveau pari avec toutes les informations sur le match  | user : int <br> ligue : int <br> match : int <br> score1 : int <br> score2 : int |
| updatePoints (user, ligue, match, score1, score2) | PUT | http://localhost:8084/FBLBack/rest/points/put?user=x&ligue=x&match=x&score1=x&score2=x | Modification d'un pari | user : int <br> ligue : int <br> match : int <br> score1 : int <br> score2 : int |
| deletePoints (user, ligue, match) | DELETE | http://localhost:8084/FBLBack/rest/points/delete?user=x&ligue=x&match=x | Suppression d'un pari | user : int <br> ligue : int <br> match : int |


### Pour les matchs joués par un user

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getUsers_matchs (user) | GET | http://localhost:8084/FBLBack/rest/user_matchs?user=xx | Liste des matchs joués par un utilisateur avec les informatiosn sur le pari effectué dans chaque match | user : int |

### Pour les ligues dans lesquelles joue un user

| Nom de la méthode | Type de requête | Lien REST | Description de l'action | Paramètres de requête|
| --- | --- | --- | --- | --- |
| getUsers (user) | GET | http://localhost:8084/FBLBack/rest/user_ligues?user=xx | Liste des ligues dans lesquels un utilisateur pari avec les informations sur ses points dans la ligue et sur le nombre de matchs joués | user : int |





